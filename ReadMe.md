# PartKeepr to Homebox

Copy data from [PartKeepr](https://partkeepr.org/) to [Homebox](https://hay-kot.github.io/homebox/).

The tool helps in migrating from PartKeepr for inventory management to Homebox by reading data from PartKeepr and inserting it into Homebox. There are limitations to this process, see below.

## Building

Standard way of building Rust applications:

    cargo build

## Running

There are some command line options but the most basic invocation is:

    ./partkeepr-to-homebox --partkeepr-url http://user:password@partkeepr --homebox-url http://homebox --homebox-user user --homebox-password password

## Supported Data

There is no 1:1 mapping and not everything was needed, which means only some fields are being copied. The details are documented in this section. A ✔ means this data is being copied, a ✖ means this data isn't being copied and ❏ designates data which isn't supported by Homebox.

For storage locations:

* ✔ Name
* ❏ Image
* ✖ hierarchy (via categories)

Storage locations without parts are also copied.

For parts:

* Basic Data
  * ✔ Name
  * ✔ Description
  * ❏ Minimum Stock
  * ❏ Measurement Unit
  * ✔ Category (as Labels, without hierarchy)
  * ✔ Storage Location
  * ❏ Footprint
  * ✔ Comment (as Notes)
  * ❏ Production Remarks
  * ✔ Status (as Labels)
  * ✔ Nees Review (as Label)
  * ❏ Condition
  * ✖ Internal Part Number (could be Asset ID)
* ✔ Stock Level
* ❏ Stock History
* ❏ Distributers
* ❏ Manufacturers
* ❏ Part Parameters
* ✔ Attachments
