use anyhow::{Error, Result};
use serde::Deserialize;
use serde::Serialize;
use url::Url;

#[derive(Debug, Deserialize)]
struct LoginResponse {
    token: String,
}

/// Context for interacting with Homebox
pub struct Homebox {
    base_url: Url,
    client: reqwest::blocking::Client,
    token: String,
}

#[derive(Debug, Serialize)]
pub struct Location {
    pub name: String,
    pub description: String,
}

/// Response where only the returned ID is needed
#[derive(Debug, Deserialize)]
struct IdResponse {
    id: String,
}

#[derive(Debug, Serialize)]
pub struct Label {
    pub name: String,
    pub description: String,
}

#[derive(Debug, Serialize)]
struct NewItem {
    name: String,
    description: String,
    #[serde(rename = "locationId")]
    location_id: String,
    #[serde(rename = "labelIds")]
    label_ids: Vec<String>,
}

#[derive(Debug, Serialize)]
struct UpdateItem {
    id: String,
    name: String,
    description: String,
    #[serde(rename = "locationId")]
    location_id: String,
    #[serde(rename = "labelIds")]
    label_ids: Vec<String>,
    notes: String,
    quantity: u64,
}

#[derive(Debug, Serialize, Clone)]
pub struct Item {
    pub name: String,
    pub description: String,
    #[serde(rename = "locationId")]
    pub location_id: String,
    #[serde(rename = "labelIds")]
    pub label_ids: Vec<String>,
    pub notes: String,
    pub quantity: u64,
}

impl From<Item> for NewItem {
    fn from(item: Item) -> Self {
        let Item {
            name,
            description,
            location_id,
            label_ids,
            ..
        } = item;
        NewItem {
            name,
            description,
            location_id,
            label_ids,
        }
    }
}

impl From<Item> for UpdateItem {
    fn from(item: Item) -> Self {
        let Item {
            name,
            description,
            location_id,
            label_ids,
            notes,
            quantity,
        } = item;
        UpdateItem {
            id: Default::default(),
            name,
            description,
            location_id,
            label_ids,
            notes,
            quantity,
        }
    }
}

impl Homebox {
    pub fn new(mut base_url: Url, user: String, password: String) -> Result<Homebox> {
        if base_url.cannot_be_a_base() {
            return Err(Error::msg(
                "Homebox URL is a cannot-be-a-base URL. Require URL to which paths can be added.",
            ));
        }
        if base_url.path_segments().expect("can-be-a-base URL").last() != Some("") {
            base_url.path_segments_mut().unwrap().push("");
        }

        let login_url = base_url.join("api/v1/users/login")?;
        let params = [("username", user), ("password", password)];
        let client = reqwest::blocking::Client::new();
        let login_response: LoginResponse = client.post(login_url).form(&params).send()?.json()?;
        Ok(Homebox {
            base_url,
            client,
            token: login_response.token,
        })
    }

    pub fn create_location(&self, location: Location) -> Result<String> {
        let locations_url = self.base_url.join("api/v1/locations")?;
        let response: IdResponse = self
            .client
            .post(locations_url)
            .header(reqwest::header::AUTHORIZATION, self.token.clone())
            .json(&location)
            .send()?
            .json()?;

        Ok(response.id)
    }

    pub fn create_label(&self, label: Label) -> Result<String> {
        let labels_url = self.base_url.join("api/v1/labels")?;
        let response: IdResponse = self
            .client
            .post(labels_url)
            .header(reqwest::header::AUTHORIZATION, self.token.clone())
            .json(&label)
            .send()?
            .json()?;
        Ok(response.id)
    }

    pub fn create_item(&self, item: Item) -> Result<String> {
        let new_item: NewItem = item.clone().into();
        let mut update_item: UpdateItem = item.into();

        // First create item with supported fields
        let items_url = self.base_url.join("api/v1/items")?;
        let response: IdResponse = self
            .client
            .post(items_url)
            .header(reqwest::header::AUTHORIZATION, self.token.clone())
            .json(&new_item)
            .send()?
            .json()?;

        let item_id = response.id;
        update_item.id = item_id.clone();

        // Then update item with all fields
        let item_url = self.base_url.join(&format!("api/v1/items/{item_id}"))?;
        let response: IdResponse = self
            .client
            .put(item_url)
            .header(reqwest::header::AUTHORIZATION, self.token.clone())
            .json(&update_item)
            .send()?
            .json()?;
        assert_eq!(response.id, item_id);

        Ok(item_id)
    }

    pub fn upload_attachment(
        &self,
        item_id: &str,
        name: String,
        mimetype: String,
        binary: Vec<u8>,
    ) -> Result<()> {
        let attachment_url = self
            .base_url
            .join(&format!("api/v1/items/{item_id}/attachments"))?;
        let form = reqwest::blocking::multipart::Form::new()
            .text("name", name.clone())
            .part(
                "file",
                reqwest::blocking::multipart::Part::bytes(binary)
                    .mime_str(&mimetype)?
                    .file_name(name.clone()),
            );
        self.client
            .post(attachment_url)
            .header(reqwest::header::AUTHORIZATION, self.token.clone())
            .multipart(form)
            .send()?;

        Ok(())
    }
}
