use anyhow::{Error, Result};
use serde::Deserialize;
use std::io::{self, Write};
use url::Url;

/// Context for interacting with PartKeepr
pub struct PartKeepr {
    base_url: Url,
}

/// Wrapper for API responses to support pagination
#[derive(Debug, Deserialize)]
struct ApiResponse<T> {
    #[serde(rename = "hydra:nextPage")]
    next_page: Option<String>,
    #[serde(rename = "hydra:totalItems")]
    totalitems: u64,
    #[serde(rename = "hydra:member")]
    member: Vec<T>,
}

#[derive(Debug, Deserialize)]
pub struct StorageLocation {
    #[serde(rename = "@id")]
    pub partkeepr_id: String,
    pub name: String,
}

/// Parent categories aren't supported, just a placeholder
#[derive(Debug, Deserialize, Clone, Eq, Hash, PartialEq)]
pub struct UninterestingCategory {}

#[derive(Debug, Deserialize, Clone, Eq, Hash, PartialEq)]
pub struct Category {
    #[serde(rename = "@id")]
    pub partkeepr_id: String,
    pub name: String,
    pub parent: Option<UninterestingCategory>,
}

#[derive(Debug, Deserialize)]
pub struct Attachment {
    #[serde(rename = "@id")]
    pub partkeepr_id: String,
    #[serde(rename = "originalFilename")]
    pub original_filename: String,
    pub mimetype: String,
}

#[derive(Debug, Deserialize)]
pub struct Part {
    pub name: String,
    pub description: String,
    pub category: Category,
    #[serde(rename = "storageLocation")]
    pub storage_location: StorageLocation,
    pub comment: String,
    pub status: String,
    #[serde(rename = "needsReview")]
    pub needs_review: bool,
    #[serde(rename = "stockLevel")]
    pub stock_level: u64,
    pub attachments: Vec<Attachment>,
}

impl PartKeepr {
    pub fn new(
        mut base_url: Url,
        user: Option<String>,
        password: Option<String>,
    ) -> Result<PartKeepr> {
        if base_url.cannot_be_a_base() {
            return Err(Error::msg(
                "Partkeepr URL is a cannot-be-a-base URL. Require URL to which paths can be added.",
            ));
        }

        if let Some(user) = user {
            base_url
                .set_username(&user)
                .map_err(|_| Error::msg("Putting username in Partkeepr URL failed"))?;
        }
        if let Some(password) = password {
            // Use `if` to keep password in URL when none was explicitly provided.
            base_url
                .set_password(Some(&password))
                .map_err(|_| Error::msg("Putting password in Partkeepr URL failed"))?;
        }

        if base_url.path_segments().expect("can-be-a-base URL").last() != Some("") {
            base_url.path_segments_mut().unwrap().push("");
        }

        Ok(PartKeepr { base_url })
    }

    pub fn read_storage_locations(&self) -> Result<Vec<StorageLocation>> {
        println!("Reading all storage locations from PartKeepr ...");
        let mut storage_locations_url = self.base_url.join("api/storage_locations")?;
        let mut storage_locations: Vec<StorageLocation> = Vec::new();
        loop {
            let mut response: ApiResponse<StorageLocation> =
                reqwest::blocking::get(storage_locations_url)?.json()?;
            storage_locations.append(&mut response.member);
            print!(
                "\rRead {} of {} storage locations",
                storage_locations.len(),
                response.totalitems
            );
            io::stdout().flush()?;
            if let Some(next_page) = response.next_page {
                storage_locations_url = self.base_url.join(&next_page)?;
            } else {
                break;
            }
        }
        println!();

        Ok(storage_locations)
    }

    pub fn read_parts(&self) -> Result<Vec<Part>> {
        println!("Reading all parts from PartKeepr ...");
        let mut parts: Vec<Part> = Vec::new();
        let mut parts_url = self.base_url.join("api/parts")?;
        loop {
            let mut response: ApiResponse<Part> = reqwest::blocking::get(parts_url)?.json()?;
            parts.append(&mut response.member);
            print!("\rRead {} of {} parts", parts.len(), response.totalitems);
            io::stdout().flush()?;
            if let Some(next_page) = response.next_page {
                parts_url = self.base_url.join(&next_page)?;
            } else {
                break;
            }
        }
        println!();

        Ok(parts)
    }

    pub fn get_file(&self, attachment: &Attachment) -> Result<Vec<u8>> {
        let source_url = self
            .base_url
            .join(&format!("{}/getFile", attachment.partkeepr_id))
            .unwrap();
        Ok(reqwest::blocking::get(source_url)?.bytes()?.to_vec())
    }
}
