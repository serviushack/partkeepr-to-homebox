mod homebox;
mod partkeepr;

use anyhow::Result;
use clap::Parser;
use std::collections::{HashMap, HashSet};
use std::io::{self, Read, Write};
use url::Url;

#[derive(Parser)]
pub struct Args {
    /// Base URL of PartKeepr (where the UI resides)
    #[arg(long)]
    partkeepr_url: Url,
    /// Optional username for PartKeepr if none is specified in the base URL
    #[arg(long)]
    partkeepr_user: Option<String>,
    /// Optional password for PartKeepr if none is specified in the base URL
    #[arg(long)]
    partkeepr_password: Option<String>,
    /// Base URL of Homebox (where the UI resides)
    #[arg(long)]
    homebox_url: Url,
    /// Username for Homebox
    #[arg(long)]
    homebox_user: String,
    /// Password for Homebox
    #[arg(long)]
    homebox_password: String,
}

fn main() -> Result<()> {
    let args = Args::parse();

    let partkeepr = partkeepr::PartKeepr::new(
        args.partkeepr_url,
        args.partkeepr_user,
        args.partkeepr_password,
    )?;

    let homebox =
        homebox::Homebox::new(args.homebox_url, args.homebox_user, args.homebox_password)?;

    // Read data from PartKeepr
    let storage_locations = partkeepr.read_storage_locations()?;
    let parts = partkeepr.read_parts()?;

    // Determine labels to create
    let mut used_categories: HashSet<partkeepr::Category> = HashSet::new();
    let mut used_status: HashSet<String> = HashSet::new();
    let mut has_needs_review = false;
    for part in &parts {
        if part.category.parent.is_some() {
            used_categories.insert(part.category.clone());
        }
        if !part.status.is_empty() {
            used_status.insert(part.status.clone());
        }
        has_needs_review |= part.needs_review;
    }
    println!(
        "Will create {} labels for {} status texts, {} categories {} 'needs review' marker:",
        used_categories.len() + used_status.len() + if has_needs_review { 1 } else { 0 },
        used_status.len(),
        used_categories.len(),
        if has_needs_review {
            "and the"
        } else {
            "but no"
        }
    );
    for status in &used_status {
        print!("{}   ", status);
    }
    for category in &used_categories {
        print!("{}   ", category.name);
    }
    if has_needs_review {
        print!("needs review   ");
    }
    println!();

    // Final chance to abort copying data
    print!("If this looks reasonable press any key to continue ...");
    io::stdout().flush()?;
    let _ = io::stdin().read(&mut [0u8]).unwrap();

    // Create locations and keep track of matching IDs in PartKeepr and Homebox
    println!("Creating locations in Homebox ...");
    let mut location_mapping: HashMap<String, String> = HashMap::new();
    let storage_locations_count = storage_locations.len();
    for (i, storage_location) in storage_locations.into_iter().enumerate() {
        let location = homebox::Location {
            name: storage_location.name,
            description: String::new(),
        };
        let location_id = homebox.create_location(location)?;
        let r = location_mapping.insert(storage_location.partkeepr_id, location_id);
        print!(
            "\rCreated {} of {} locations",
            i + 1,
            storage_locations_count
        );
        io::stdout().flush()?;
        assert!(r.is_none());
    }
    println!();

    // Create labels and keep track of matching IDs in PartKeepr and Homebox
    println!("Creating status labels in Homebox ...");
    let mut status_mapping: HashMap<String, String> = HashMap::new();
    let used_status_count = used_status.len();
    for (i, status) in used_status.into_iter().enumerate() {
        let label = homebox::Label {
            name: status.clone(),
            description: String::new(),
        };
        let label_id = homebox.create_label(label)?;
        let r = status_mapping.insert(status, label_id);
        assert!(r.is_none());
        print!("\rCreated {} of {} labels", i + 1, used_status_count);
        io::stdout().flush()?;
    }
    println!();

    println!("Creating category labels in Homebox ...");
    let mut category_mapping: HashMap<String, String> = HashMap::new();
    let used_categories_count = used_categories.len();
    for (i, category) in used_categories.into_iter().enumerate() {
        let label = homebox::Label {
            name: category.name,
            description: String::new(),
        };
        let label_id = homebox.create_label(label)?;
        let r = category_mapping.insert(category.partkeepr_id, label_id);
        assert!(r.is_none());
        print!("\rCreated {} of {} labels", i + 1, used_categories_count);
        io::stdout().flush()?;
    }

    let needs_review_id = if has_needs_review {
        println!("Creating 'needs review' label in Homebox ...");
        let label = homebox::Label {
            name: "needs review".to_owned(),
            description: String::new(),
        };
        let label_id = homebox.create_label(label)?;
        println!("Creating label");
        Some(label_id)
    } else {
        None
    };

    // Create items and their attachments
    println!("Creating items in Homebox ...");
    let parts_count = parts.len();
    for (i, part) in parts.into_iter().enumerate() {
        print!(
            "\rCreating item {} of {}                                ",
            i + 1,
            parts_count
        );
        io::stdout().flush()?;
        let category = category_mapping
            .get(&part.category.partkeepr_id)
            .into_iter();
        let status = status_mapping.get(&part.status).into_iter();
        let needs_review = if part.needs_review {
            assert!(needs_review_id.is_some());
            needs_review_id.as_ref()
        } else {
            None
        };
        let item = homebox::Item {
            name: part.name.clone(),
            description: part.description.clone(),
            location_id: location_mapping
                .get(&part.storage_location.partkeepr_id)
                .unwrap()
                .clone(),
            label_ids: category
                .chain(status)
                .chain(needs_review.into_iter())
                .map(|s| s.to_owned())
                .collect(),
            notes: part.comment,
            quantity: part.stock_level,
        };
        let item_id = homebox.create_item(item)?;

        let attachments_count = part.attachments.len();
        for (j, attachment) in part.attachments.into_iter().enumerate() {
            print!(
                "\rCreating item {} of {}: Dowloading attachment {} of {}",
                i + 1,
                parts_count,
                j + 1,
                attachments_count
            );
            io::stdout().flush()?;
            let binary = partkeepr.get_file(&attachment)?;
            print!(
                "\rCreating item {} of {}: Uploading attachment {} of {}",
                i + 1,
                parts_count,
                j + 1,
                attachments_count
            );
            io::stdout().flush()?;
            homebox.upload_attachment(
                &item_id,
                attachment.original_filename,
                attachment.mimetype,
                binary,
            )?;
        }
    }

    Ok(())
}
